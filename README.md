# SOR API Gem

Ruby wrapper gem for SOR API.

## Installation

Add this line to your application's Gemfile:

    gem 'sor_api'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sor_api

## Available actions

- user/create
- user/update
- user/login_token
- user/information
- user/deactivate
- user/reactivate
- user/activation_email
- user/credits


## Example
```ruby
require './lib/sor_api'

SorApi.configure do |c|
  c.base_uri = 'https://api.saveonuat.com'
  c.api_user = 'xxx'
  c.api_secret = 'xxx'
end

sor_api_client = SorApi.new

begin
  login_token = sor_api_client.login_token({"Email":"test@travelinto.com"})
rescue SorApi::Error => exception
  puts "Something went wrong: #{exception}"
end
```

### Create A User
* [Sample Requests & Additional Info](https://api.saveonresorts.com/Help/Api/POST-v2-clubmembership-createdefault)
```ruby
user = {
  "Email":"test@travelinto.com",
  "ContractNumber":"",
  "Address":"111 Main Street",
  "City":"San Diego",
  "State":"CA",
  "PostalCode":"92126",
  "TwoLetterCountryCode":"US",
  "Phone":"8888888888",
  "Password":"password123",
  "FirstName":"TestAPI",
  "LastName":"User",
  "UserAccountTypeID":"5",
  "MembershipType":"",
}
response = sor_api_client.create(user)
```

### Update Users
* [Sample Requests & Additional Info](https://api.saveonresorts.com/Help/Api/POST-v2-clubmembership-updatemembers)
```ruby
user = {
  "UserId":"4653328",
  "Email":"test@travelinto.com",
  "Updates": {
    "Email":"updatedtest@travelinto.com",
    "Password":"password123",
    "ContractNumber":"",
    "OtherId":"",
    "FirstName":"TestAPI",
    "LastName":"Updated",
    "StreetAddress":"111 Main Street",
    "City":"San Diego",
    "State":"CA",
    "PostalCode":"92126",
    "Phone":"8888888888",
    "SecondaryPhone":"8888888888",
    "ReferringUserId":"",
    "ExpirationDate":"2019-10-26T15:47:17.5016872-07:00",
    "UserAccountTypeId":"5",,
    "AutoRenew": true,
  }
}
users = {"Requests": [ user ]}
response = sor_api_client.update(users)
```

### Login Token
* Token is valid for 10 minutes once generated
* Redirect user with token `https://{WebsiteDomain}/vacationclub/logincheck.aspx?Token=XXX`

```ruby
user = {
  "Email": "test@travelinto.com",
  "Password": "123456"
}
response = sor_api_client.login_token(user)
```

### User Information
```ruby
user = {
  "Email": "test@travelinto.com"
}
users = {"MemberSearchList": [ user ]}
response = sor_api_client.information(users)
```

### Deactivate User
```ruby
user = {
  "Email": "test@travelinto.com",
  "ChangeNote": "Subscription Canceled"
}
response = sor_api_client.deactivate(user)
```

### Reactivating User
```ruby
user = {
  "Email": "test@travelinto.com",
  "ChangeNote": "Subscription Renewed"
}
response = sor_api_client.reactivate(user)
```

### Request Activation Email
```ruby
response = sor_api_client.activation_email({"Email": "test@travelinto.com"})
```

### Retrieve Reward Credits
```ruby
response = sor_api_client.credits({"Email": "test@travelinto.com"})
```



## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
