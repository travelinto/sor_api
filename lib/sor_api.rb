$:.unshift File.dirname(__FILE__)

require 'sor_api/utils'
require 'sor_api/actions/user'
require 'sor_api/client'
require 'sor_api/error'

module SorApi
  class << self
    attr_accessor :configuration

    def new
      @configuration ||= Configuration.new
      SorApi::Client.new(
        :base_uri => @configuration.base_uri,
        :api_user => @configuration.api_user,
        :api_secret => @configuration.api_secret,
        :timeout => @configuration.timeout
      )
    end

    def configure
      @configuration ||= Configuration.new
      yield(@configuration)
    end
  end

  class Configuration
    attr_accessor :base_uri, :api_user, :api_secret, :timeout

    def initialize
      # @api_user = @api_secret = 'xxx'
      @timeout = 15
    end
  end
end
