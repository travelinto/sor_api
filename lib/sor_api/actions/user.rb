module SorApi
  module Actions
    module User

      def test(*args)
        options = Utils.extract_options!(args)
        Utils.parse_response self.class.post(
          '/post', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def create(*args)
        options = Utils.extract_options!(args)
        Utils.parse_response self.class.post(
          '/v2/clubmembership/createdefault', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def update(*args)
        options = Utils.extract_options!(args)
        Utils.parse_response self.class.post(
          '/v2/clubmembership/updatemembers',
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def login_token(*args)
        options = Utils.extract_options!(args)
        Utils.set_credentials(options)
        Utils.parse_response self.class.post(
          '/clubmembership/getlogintokennovalidation', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def information(*args)
        options = Utils.extract_options!(args)
        Utils.set_credentials(options)
        Utils.parse_response self.class.post(
          '/clubmembership/getmembers', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def deactivate(*args)
        options = Utils.extract_options!(args)
        Utils.set_credentials(options)
        Utils.parse_response self.class.post(
          '/clubmembership/deactivatemember', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def reactivate(*args)
        options = Utils.extract_options!(args)
        Utils.set_credentials(options)
        Utils.parse_response self.class.post(
          '/clubmembership/activatemember', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def activation_email(*args)
        options = Utils.extract_options!(args)
        Utils.parse_response self.class.post(
          '/v2/clubmembership/requestactivationemail', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end

      def credits(*args)
        options = Utils.extract_options!(args)
        Utils.set_credentials(options)
        Utils.parse_response self.class.post(
          '/clubmembership/getmemberpointledger', 
          headers: {'Content-Type': 'application/json'}, 
          body: options.to_json
        )
      end


      Utils.define_bang_methods(self)

    end
  end
end
