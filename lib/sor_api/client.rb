require 'httparty'
require 'json'

module SorApi
  class Client

    include HTTParty
    include Actions::User

    #format :json

    # base_uri 'https://memberstravelinto.saveonuat.com'
    # base_uri 'https://api.travelinto.com'

    def initialize(*args)

      options = Utils.extract_options!(args)

      unless options[:base_uri]
        raise Utils.argument_error("no base_uri set") 
      end

      self.class.base_uri options[:base_uri]

      unless options[:api_user] || options[:api_secret]
        raise Utils.argument_error("api_user and api_secret") 
      end

      self.class.headers 'x-saveon-username': options[:api_user]
      self.class.headers 'x-saveon-secret': options[:api_secret]

      # self.class.default_params(
      #   :api_user   => options[:api_user],
      #   :api_secret => options[:api_secret]
      # )

      self.class.default_timeout(options[:timeout])
    end
  end
end
