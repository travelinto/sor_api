module SorApi
  class Error < StandardError; end
  class GatewayTimeout < StandardError; end
end
